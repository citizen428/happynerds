# frozen_string_literal: true

module CategoryHelpers
  CATEGORY_MAPPINGS = { ios: 'iOS', raspberry: 'Raspberry Pi' }.freeze

  def category_name(category)
    CATEGORY_MAPPINGS.fetch(category.to_sym) { category.capitalize }
  end
end
