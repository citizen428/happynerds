# Happy Nerds

[Happy Nerds](http://www.happynerds.net) is a web site where I collect links to various programming languages/environments, websites and books specifically targeted at kids.

It is built using [Middleman](http://middlemanapp.com) and hosted on
[Netlify](https://www.netlify.com).

## Architecture

The different category pages are created from a single template via Middleman's [proxy](https://middlemanapp.com/advanced/dynamic-pages/) method:

```ruby
# config.rb
...
%w(windows mac linux browser ios raspberry books meta).each do |category|
  proxy(
    "/sites/#{category}/index.html",
    '/sites/template.html',
    locals: { category: category },
    ignore: true
  )
end
...
```

The template then uses a [data file](https://middlemanapp.com/advanced/data-files/) to only
include sites in the respective category in each generated page:

```ruby
...
# source/sites/template.html.erb
<% sites = data.sites.select { |site| site.categories.include?(category) } %>
<% sites.each do |site| %>
    <article>
      <a href="<%= site[:url] %>"><%= site[:name] %></a>
      <p><%= site[:description] %></p>
    </article>
<% end %>
```

The sitemap generation code makes use of Middleman's [internal sitemap](https://middlemanapp.com/advanced/sitemap/):

```erb
# source/sitemap.xml.erb
---
layout: false
directory_index: false
---
<% pages = sitemap.resources.find_all { |p| p.source_file.match(/\.html/) } %>
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<% pages.each do |p| %>
  <url>
    <loc>
      https://www.happynerds.net/<%= p.destination_path.gsub('index.html', '') %>
    </loc>
  </url>
<% end %>
</urlset>
```

## History

The initial version was built with [Ruby](http://ruby-lang.org), [Sinatra](http://sinatrarb.com) and [MongoDB](https://www.mongodb.com), hosted on [Heroku](http://heroku.com) and
[mLab](https://mlab.com).

This was followed by a version using [Clojure](http://clojure.org) and
[Luminus](http://www.luminusweb.net), still backed by MongoDB and hosted on Heroku and mLab.

The next iteration was done with [Ruby on Rails](http://rubyonrails.org) and [PostgreSQL](https://www.postgresql.org), and was the last version of the site to run on Heroku.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/citizen428/happynerds. If
you want to add another site, add its information to `data/sites.json`.

## License

The code is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
